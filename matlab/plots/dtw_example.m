function aligned_data = dtw_example
    addpath('..');
    
    data = cell(2);
    data{1} = read_data('../../../BatLevelLogs/042335963089f20b/bike1-20141110_165404.csv');
    data{2} = read_data('../../../BatLevelLogs/042335963089f20b/bike2good-20141110_173731.csv');
    
    power = cell(2);
    for i = 1:2
        power{i} = preprocess_data(data{i}.Power, ...
            'SmoothingWindow', 200, 'DownsampleFactor', 10);
    end
    
    aligned_data = align_data(power, true);
end