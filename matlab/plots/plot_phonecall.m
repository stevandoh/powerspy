function plot_phonecall
    DEVICE = '0094e779d7d1841f';
    addpath('..');
    LOG_DIR = '../../../BatLevelLogs';
    FILENAME = 'phonecall-20141107_001123.csv';
    
    data = read_data([LOG_DIR '/' DEVICE '/' FILENAME]);
    preprocessed_power = smooth(data.Power, 200);
    range = 600:(length(data.Power) - 300);
    plot(data.RelativeTime(range) / 1000, preprocessed_power(range));
    xlabel('Time [sec]', 'FontSize', 15); 
    ylabel('Power [Watt]', 'FontSize', 15);
    axis tight;
    set(gca, 'box', 'off');
end