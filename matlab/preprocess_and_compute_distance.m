function [distance_matrix, preprocessed_data] = ...
    preprocess_and_compute_distance(log_list)

    data_field_name = 'Power';
    SMOOTHING_WINDOW = 200;
    DS_FACTOR = 10; % Downsampling factor

    % preprocess all data in log table
        preprocessed_data = cell(length(log_list), 1);
        for i = 1:length(log_list)
            data = log_list.Data{i};

            preprocessed_data{i} = ...
               preprocess_data(data.(data_field_name), ...
                    'SmoothingWindow', SMOOTHING_WINDOW, ...
                    'DownsampleFactor', DS_FACTOR);
        end

        % calculate DTW distance between every pair of sequences
        distance_matrix = compute_dtw_distance_matrix(preprocessed_data);

end