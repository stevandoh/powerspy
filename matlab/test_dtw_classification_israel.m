function mcr = test_dtw_classification_israel
    [log_list, ~] = read_log_list('israel_logs.csv');
    
    filter_field_name = 'Current';

    % filter devices that support signal strength measurement
    log_filter = strcmp(log_list.(filter_field_name), 'Yes') & ...
                 ~strcmp(log_list.Charging, 'Yes') & ...
                 (strcmp(log_list.Route, 'Vrd2Raf') | ...
                  strcmp(log_list.Route, 'Raf2Vrd'));
%                  (strcmp(log_list.Route, 'Tlv2Hfa') | ...
%                   strcmp(log_list.Route, 'Hfa2Tlv'));
    
    filtered_log_list = log_list{log_filter, :};
    log_list = cell2table(filtered_log_list, 'VariableNames', ...
        log_list.Properties.VariableNames);
    fprintf('%d entries in filtered log list\n', height(log_list));
    display(unique(log_list.Route));
    
    mcr = evaluate_dtw_classification(log_list);
end