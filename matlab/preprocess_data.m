function data_out = preprocess_data(data_in, varargin)
    SMOOTHING_WINDOW = 20;
    DOWNSAMPLE_FACTOR = 1;
    
    if nargin > 1
       % handle variable arguments
       for i = 1:length(varargin)
          if strcmp(varargin{i}, 'SmoothingWindow')
             SMOOTHING_WINDOW = varargin{i+1}; 
          end
          
          if strcmp(varargin{i}, 'DownsampleFactor')
             DOWNSAMPLE_FACTOR = varargin{i+1}; 
          end
       end
    end

    data = smooth(data_in, SMOOTHING_WINDOW);
    data = standardize_data(data);
    data_out = downsample(data, DOWNSAMPLE_FACTOR);
end

function std_data = standardize_data(data)
    data = remove_dc_offset(data);
    std_data = data / std(data);
end