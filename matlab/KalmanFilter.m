classdef KalmanFilter < handle
    % Kalman Filter implementation
        
    properties
        state
        velocity
    end
    
    properties (SetAccess = protected, GetAccess = public)
        prev_state
        P       % Covariance matrix
        F_matrix
        R
        K       % Kalman parameter
        Q       % error term
        H
    end
        
    methods
        function obj = KalmanFilter(init_state)
            % class constructor
            obj.state = init_state; 
            obj.velocity = [0 0];
            obj.P = [1 0; 0 1];
            obj.F_matrix = [1 0; 0 1];
            obj.R = [0.1 0; 0 0.1];
            obj.K = [0.2 0; 0 0.2];
            obj.Q = [0.1 0; 0 0.1];
            obj.H = [1 0; 0 1];
        end
        
        function set_initial_state(obj, state)
            obj.state = state;
        end
        
        function set_initial_velocity(obj, velocity)
            obj.velocity = velocity;
        end
        
        function [x, y] = estimate_loc(obj, observation)
            obj.predict();
            obj.update(observation);
            x = obj.state(1);
            y = obj.state(2);
        end
    end
    
    methods(Access = protected)
        function predict(obj)
            obj.prev_state = obj.state; 
            obj.state = obj.state + obj.velocity;
            obj.P = obj.F_matrix * obj.P * transpose(obj.F_matrix) + obj.Q;
        end
        
        function update(obj, observation)
            Yk = observation - obj.state * obj.H;
            Sk = obj.P + obj.R;
%             if observation(1) - obj.prev_state(1) > 3 * obj.velocity(1) || ...
%                     observation(2) - obj.prev_state(2) > 3 * obj.velocity(2)
%                 obj.state = obj.state;
%             else 
%                 obj.state = obj.state + Yk * obj.K;
%             end
            if dot(obj.velocity, (observation - obj.prev_state)) < 0
                obj.velocity = [0 0];
            else               
                obj.state = obj.state + Yk * obj.K;
                obj.velocity = obj.state - obj.prev_state;

            end
            obj.K = obj.P * obj.H * inv(Sk);
            obj.P = (eye(2) - obj.K * obj.H) * obj.P;
            disp(obj.P);
        end
    end
end
