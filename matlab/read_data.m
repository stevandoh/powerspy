function data = read_data(filename)
    data = readtable(filename, 'Delimiter', 'tab');
    % Add a power column
    if any(strcmp(data.Properties.VariableNames, 'Current'))
        power = get_power(data);
        data.Power = power;
    end
    % plot3(data.Latitude, data.Longitude, power);
    % grid;
    
    data.RelativeTime = data.Time - data.Time(1);
end

function power = get_power(data)
    % uV and uA units are used in recordings therefore multiplying by 1e-12
    % to get Watt
    power = data.Volt .* data.Current * 1e-12;
end