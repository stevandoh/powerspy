function [d, a, b] = search_subsequence(profile, subseq, algorithm)
    % Search a subsequence within a profile
    % profile - Large sequence to search in
    % subseq - The subsequence to look for
    % algorithm - "dtw" or "osb"
    
    if length(profile) > length(subseq)
        warp_window = floor(length(subseq) / 10);
        if strcmp(algorithm, 'osb')
          [d, a, b] = optimal_subsequence_bijection_c(subseq, profile, warp_window, 3, 3, 0.1);
        else
          [d, a, b] = dtw_subsequence_c(profile, subseq, warp_window);
        end
        d = d / length(subseq);
    else
        warp_window = floor(length(profile) / 10);
        d = get_dtw_distance(profile, subseq, warp_window);
        a = 1;
        b = length(profile);
    end
end
