classdef ParticleFilter < handle
    % Particle filter implementation
    
    properties (GetAccess = protected)
        data;
        num_particles;
        velocity;
    end
    
    properties (SetAccess = protected, GetAccess = public)
        offset;
        routes;
        weights;
    end
    
    methods
        function obj = ParticleFilter(data, num_particles)
           % Class constructor
           obj.data = data;
           obj.num_particles = num_particles;
           obj.routes = randi(length(obj.data), obj.num_particles, 1);
           obj.offset = zeros(size(obj.routes));
           obj.velocity = ones(obj.num_particles, 1);
           
           for i = 1:length(obj.data)
               ind = obj.routes == i;
               obj.offset(ind) = randi(length(obj.data{i}), sum(ind), 1); 
           end
           
           obj.weights = ones(obj.num_particles, 1) ./ obj.num_particles;
        end
        
        function update(obj, observation, step_size)
            % Move particle indices and update weights according to data
            % and expected measurement
            
            % Move particles
            for i = 1:length(obj.data)
                ind = obj.routes == i;
                obj.offset(ind) = min(obj.offset(ind) + round(obj.velocity(ind) * step_size), ...
                    length(obj.data{i}));
            end
            
            % particle positions cannot be outside valid index range
            
            obj.calc_weights(observation);
            obj.resample();
            % obj.rerandomize();
        end % update
    end
    
    methods(Access = protected)
        
        function Neff = calc_weights(obj, observation)
            % Calculate particle weights based on difference 
            % between expected measurement for each particle and
            % actual measurement
            distances = inf(obj.num_particles, 1);
            for i = 1:length(obj.data)
                ind = obj.routes == i;
                distances(ind) = abs(obj.data{i}(obj.offset(ind)) - observation);
            end
            
            obj.weights = exp(- distances .^ 2 / 1) .* obj.weights;
            
            % Obtain resampling probabilities by normalizing weights
            obj.weights = obj.weights / sum(obj.weights);
            Neff = 1 / sum(obj.weights .^ 2);
            
%             display(Neff);
            if Neff < 0.15 * obj.num_particles
                fprintf('Resampling\n');
                obj.reshuffle();
            end
%             display(max(obj.weights));
        end
        
        function reshuffle(obj)
            obj.routes = randi(length(obj.data), obj.num_particles, 1);
            obj.offset = zeros(size(obj.routes));
            obj.velocity = ones(obj.num_particles, 1);
           
            for i = 1:length(obj.data)
                ind = obj.routes == i;
                obj.offset(ind) = randi(length(obj.data{i}), sum(ind), 1); 
            end
           
            obj.weights = ones(obj.num_particles, 1) ./ obj.num_particles;
        end
        
        function resample(obj)
            % Resample particles
            particle_range_end = cumsum(obj.weights);
            new_particle_indices = zeros(obj.num_particles, 1);
            
            r = rand(obj.num_particles, 1);
            for i = 1:obj.num_particles
                new_particle_ind = find(particle_range_end >= r(i), 1,'first');
                new_particle_indices(i) = new_particle_ind;
            end
            
            obj.offset = obj.offset(new_particle_indices);
            obj.routes = obj.routes(new_particle_indices);
%             obj.velocity = max(0, (1.6 - obj.weights) .* rand(obj.num_particles, 1));
            obj.velocity = max( ...
                obj.velocity(new_particle_indices) ...
                + 0.1 .* randn(obj.num_particles, 1) ...
                , 0);
        end % resample
    end
end

