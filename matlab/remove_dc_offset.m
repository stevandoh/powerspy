function output = remove_dc_offset(input)
%     a = [1 , -0.99]; b = [1,-1];
%     output = filtfilt(b, a, input);
    output = input - mean(input);
end