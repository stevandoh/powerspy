function test_tracking_using_segments
    RECORD_MOVIE = false;
    
    test_file = '../../BatLevelLogs/0094e779d7d1841f/tosb-20140623_131114.csv';
    log_list = get_log_list(test_file);
        
    test_data = read_data(test_file);
    
    test_profile = create_profile(test_data);
    
    seg_center_coords = get_seg_center_coords(log_list);
    
    segments = unique(log_list.Route);
    
    h = figure;
    WINDOW_SIZE = 2000;
    STEP_SIZE = 1000;
    
    ind_range = 1:floor((length(test_profile) - WINDOW_SIZE)/STEP_SIZE);
    
    if RECORD_MOVIE
        movie_frames(length(ind_range)) = struct('cdata',[],'colormap',[]);
    end
    
    for i = ind_range
        position = 1+(i-1)*STEP_SIZE;

        lon = test_profile(position, 2);
        lat = test_profile(position, 3);
        
        subseq = test_profile(position:position + WINDOW_SIZE, 4);
        
        distance_matrix = compute_distances(log_list, subseq);
        segment_ind = find_most_likely_segment(distance_matrix);
        display(segment_ind);
        
        est_lon = seg_center_coords(segment_ind, 1);
        est_lat = seg_center_coords(segment_ind, 2);
        
        segment_indices = find(strcmp(segments{segment_ind}, log_list.Route));
        
        est_segment = [log_list.Data{segment_indices(1)}.Longitude, ...
            log_list.Data{segment_indices(1)}.Latitude];
        
        display([est_lat est_lon]);
        
        plot_est_segment(h, lon, lat, ...
            test_profile, est_segment);
        
        if RECORD_MOVIE
            movie_frames(i) = getframe(h);
        end
    end
    
    if RECORD_MOVIE
        movie2avi(movie_frames, 'tracking.avi');
    end
end

function plot_est_segment(h, true_lon, true_lat, profile, est_segment)
    figure(h);
    scatter(profile(:, 2), profile(:, 3));
    hold all;
    
    scatter(est_segment(:, 1), est_segment(:, 2));
    scatter(true_lon, true_lat, 300, 'fill');
    
    % plot location guess
    hold off;
    legend('Profile', 'Estimated', 'True');
end

function seg_center_coords = get_seg_center_coords(log_list)
    % get the coordinates (lon, lat) of the centers of the segments
    segments = unique(log_list.Route);
    seg_center_coords = nan( length(segments), 2);
        
    for i = 1:length(segments)
        seg_ind = strcmp(log_list.Route, segments{i});
        ref_seg = log_list.Data{seg_ind};
        seg_center_coords(i, 1) = mean(ref_seg.Longitude);
        seg_center_coords(i, 2) = mean(ref_seg.Latitude);
    end
end

function segment_ind = find_most_likely_segment(distance_matrix)
    distance_vector = inf(length(distance_matrix), 1);
    for i = 1:length(distance_vector)
        distance_vector(i) = min(distance_matrix{i});
    end
    
    [distance, segment_ind] = min(distance_vector);
    display(distance);
end

function distance_matrix = compute_distances(log_list, subseq)
    % compute DTW distances between subsequence and each test
    % profile in data
    segments = unique(log_list.Route);
    
    distance_matrix = cell(length(segments), 1);
    
    for i = 1:length(segments)
        ref_seg_ind = find(strcmp(log_list.Route, segments{i}));
        distance_vector = inf( length(ref_seg_ind), 1);
        
        for j = 1:length(distance_vector)
            preprocessed_data = preprocess_data( log_list.Data{ref_seg_ind(j)}.Power, ...
                'SmoothingWindow', 100);
            distance_vector(j) = ...
                get_dtw_distance(preprocessed_data, subseq);
        end
        
        distance_matrix{i} = distance_vector;
    end
end

function log_list = get_log_list(test_filename)
    log_list = read_log_list('tosb_segments.csv', '../../data');

    test_file_dir = fileparts(test_filename);
    
    log_filter = strcmp(log_list.GPS, 'Yes') ...
        & strcmp(log_list.Charging, 'No');
    % & isempty(strcmp(log_list.Filename, test_file_dir)) ...

    filtered_log_list = log_list{log_filter, :};
    log_list = cell2table(filtered_log_list, 'VariableNames', ...
        log_list.Properties.VariableNames);

    fprintf('%d entries in filtered log list\n', length(log_list));
end

function profile = create_profile(data)
    preprocessed_power = preprocess_data(data.Power, ...
        'SmoothingWindow', 100);
    profile = [data.RelativeTime data.Longitude data.Latitude preprocessed_power];
end
