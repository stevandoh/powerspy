import os
import glob
import random
import mlpy
from itertools import product
import numpy
from pprint import pprint
import operator

N = 1000
MAX_INTERSECTIONS = 8
T = MAX_INTERSECTIONS + 1 #number of steps to execute the filter
Particles = []
Weights = []
CacheWeights = {}

indirs = [ "../cut-segments/segments/AhuzaOct31-20141031_180252/smooth", "../cut-segments/segments/AhuzaOct20-20141020_093624/smooth" ]
#indirs = [ "../cut-segments/segments/AhuzaOct20-20141020_093624/smooth" ]

trackfile = "../cut-segments/vrd-yotam-horev-pica-vrd-20141031_191430.csv"
#trackfile = "../cut-segments/vrd-haguy-yotam-sefer-carm-vrd-20141031_192557.csv"

start_line = 6201 #The line at which the track actually starts in trackfile
#start_line = 4922 #The line at which the track actually starts in trackfile
segments = [ [ 0 for x in xrange(MAX_INTERSECTIONS) ] for x in xrange(MAX_INTERSECTIONS) ] #The topology of the segments
time_per_segment = [ [ 0 for x in xrange(MAX_INTERSECTIONS) ] for x in xrange(MAX_INTERSECTIONS) ] #The parameters of the distribution of the time it takes to travel over a segment (normal dist.).
ref_array = {} # The reference power profiles for each segment
track = []     # The track to be examined
time_quantum = 3000.0 # in mili-seconds
times_access = {} # The indexes of the track for each time (rounded up to the nearest time_quantum)
norm_subtracks  = {} # Normalized every possible subtrack of track (to save time).
t_max = 0 # The total time of the track. Set in input_track().
D_min = int(40000/time_quantum) #The minimal traversal time over a segment
D_max = int(210000/time_quantum) #The minimal traversal time over a segment
WeighFunction = "min" # "avg" 

random.seed()

def init_segments():
	segments[1][7] = segments[1][2] = 1
	segments[2][1] = segments[2][3] = segments[2][4] = 1
	segments[3][2] = segments[3][4] = 1
	segments[4][2] = segments[4][3] = segments[4][5] = 1
	segments[5][4] = segments[5][6] = 1
	segments[6][4] = segments[6][5] = segments[6][7] = 1
	segments[7][1] = segments[7][6] = 1
	
	time_per_segment[1][7] = time_per_segment[7][1] = (60000/time_quantum, 7000/time_quantum)
	time_per_segment[1][2] = time_per_segment[2][1] = (180000/time_quantum, 7000/time_quantum)
	time_per_segment[2][3] = time_per_segment[3][2] = (60000/time_quantum, 7000/time_quantum)
	time_per_segment[3][4] = time_per_segment[4][3] = (120000/time_quantum, 7000/time_quantum)
	time_per_segment[4][5] = time_per_segment[5][4] = (120000/time_quantum, 7000/time_quantum)
	time_per_segment[5][6] = time_per_segment[6][5] = (60000/time_quantum, 7000/time_quantum)
	time_per_segment[6][7] = time_per_segment[7][6] = (60000/time_quantum, 7000/time_quantum)
	time_per_segment[6][4] = (180000/time_quantum, 7000/time_quantum)
	time_per_segment[4][2] = time_per_segment[2][4] = (70000/time_quantum, 7000/time_quantum)
	

def init_ref_array():
	for x, y, z in product(xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS)):
		if segments[x][y] == 1 and segments[y][z] == 1 and x != z:
			filename = str(x) + "_" + str(y) + "_" + str(z)
			powerlist = []
			distlist = []
			for dir in indirs:
				for sfile in glob.glob(os.path.join(dir, filename + "*.csv")):
					power = []
					#filepath = os.path.join(dir, sfile)
					sf = open(sfile, 'r')
					for line in sf:
						values = line.split('\t')
						try:
							time = float(values[0]) #just to check if this is a valid line
						except ValueError:
							continue
						power.append(values[1])
					sf.close()
					for i in powerlist:
						distlist.append(mlpy.dtw_std(i, power)/min(len(i), len(power)))
					powerlist.append(power)
			if (len(distlist) > 0):
				ref_array[filename] = [powerlist, 1.0]#sum(distlist)/len(distlist)]
			else:
				ref_array[filename] = [powerlist, 1.0]

def input_track():
	global t_max
	f = open(trackfile, 'r')
	next_time = 0
	start_time = None
	i = 0
	for line in f:
		values = line.split('\t')
		try:
			time = float(values[0])
		except ValueError:
			continue
		track.append(float(values[2])*float(values[3]))
		if i > start_line:
			if start_time is None:
				start_time = time
			if time - start_time >= next_time*time_quantum:
				times_access[next_time] = i
				next_time += 1
		i += 1
	t_max = int(next_time - 1)
	print t_max
	
	#normalize each possible interval in the track
	for t_start in range(t_max-D_min):
		#print str(t_start)
		for t_end in range(t_start + D_min, min(t_max, t_start + D_max)):
			timename = str(t_start)+ "-" + str(t_end)
			start_i = times_access[t_start]
			end_i = times_access[t_end]
			subtrack = track[start_i:end_i]
			#Normalize
			mean = numpy.mean(numpy.array(subtrack))
			std = numpy.std(numpy.array(subtrack))
			for i in range(len(subtrack)):
				subtrack[i] = (subtrack[i] - mean)/std
			#cut percentile
			perc_avg_power = numpy.percentile(numpy.array(subtrack), 90) #90% gave the best results
			for i in range(len(subtrack)):
				if subtrack[i] < perc_avg_power:
					subtrack[i] = 0
			norm_subtracks[timename] = subtrack
	#pprint(norm_subtracks)

def PrintHistogram(p,maxl):
	track_hist = {}
	#maxl = MAX_INTERSECTIONS + 2#maximum length of a route
	rlist = []
	for i in p:
		r = ReadRoute(i)
		while len(r) < maxl:
			r.append('0')
		s = '-'.join(r)
		if s in track_hist:
			track_hist[s] += 1
		else:
			track_hist[s] = 1
	pprint(track_hist)
	
	for route in track_hist:
		r = route.split('-')
		rlist.append(r)
	top_number = 5
	top_routes = [ [ [] for x in xrange(top_number) ] for x in xrange(maxl+1) ]
	top_routes[0][0] = []
	for l in range(maxl):
		#print "l=" +str(l)
		found_top_routes = False
		n1 = 0
		pnum = 0
		found_enough = False
		while not found_enough:
			for n in range(top_number):
				#print "n=" + str(n)
				#print top_routes[l][n]
				candidates = {}
				for r in rlist:
					if (cmp(top_routes[l][n], r[:l]) == 0):
						#print "found candidate " + str(r[l])
						if r[l] in candidates:
							candidates[r[l]] += track_hist['-'.join(r)]
						else:
							candidates[r[l]] = track_hist['-'.join(r)]
				#print "candidates:"	
				sorted_candidates = sorted(candidates.items(), key=operator.itemgetter(1)) #sort the candidates by their values
				sorted_candidates.reverse()
				#print sorted_candidates
				for i in sorted_candidates:
					top_routes[l+1][n1] = top_routes[l][n] + list(i[0])
					pnum += i[1]
					#print "new top list:"
					#print top_routes[l+1][n1]
					n1 += 1
					if n1 == top_number or pnum == N:
						#print "found enough top lists"
						found_enough = True;
						break;
				if found_enough:
					break;
	pprint(top_routes[maxl-1])
		
			
	

def WeighRoute(route):
	last_segment = route[-1]
	before_last_segment = route[-2]
	z = last_segment[1]
	y = last_segment[0]
	x = before_last_segment[0]
	t_start = int(before_last_segment[2])
	t_end = int(last_segment[2])
	
	segmentname = str(x) + "_" + str(y) + "_" + str(z)
	powerreflist = ref_array[segmentname][0]
	timename = str(t_start)+ "-" + str(t_end)
	subtrack = norm_subtracks[timename]
	dist = []
	for powerref in powerreflist:
		dist.append(mlpy.dtw_std(subtrack, powerref)/min(len(subtrack), len(powerref)))
	if WeighFunction == "avg":
		val = sum(dist)/len(dist)
	elif WeighFunction == "min":
		val = min(dist)
	return  1/(val/ref_array[segmentname][1])

def Move(t):
	global Particles
	global Weights
	ptemp = []
	for i in range(N):
		#print str(t)+ str(i)
		route = list(Particles[t-1][i])
		last_segment = route[-1]
		if t_max - last_segment[2] > D_min:
			while True:
				z = random.randrange(MAX_INTERSECTIONS)
				if segments[last_segment[1]][z] == 1 and last_segment[0] != z:
					break;
			
			mean_time_to_finish_segment = last_segment[2] + time_per_segment[last_segment[1]][z][0]
			segstr = str(last_segment[1])+"-"+str(z)+"-"+str(last_segment[2])
			if segstr in CacheWeights:
				tm = CacheWeights[segstr][0]
				mw = CacheWeights[segstr][1]
			else:
				mw = 0
				for t1 in range(min(t_max-1,int(mean_time_to_finish_segment - 2*time_per_segment[last_segment[1]][z][1])),min(t_max,int(mean_time_to_finish_segment + 2*time_per_segment[last_segment[1]][z][1]))):
					route.append([last_segment[1], z, t1])
					w = WeighRoute(route)
					if  w > mw:
						mw = w
						tm = t1
					route.pop()
				CacheWeights[segstr] = [tm, mw]
			route.append([last_segment[1], z, tm])
			Weights[i] = mw
			#travel_time = round(random.gauss(time_per_segment[last_segment[1]][z][0], time_per_segment[last_segment[1]][z][1]))
			#travel_time = min(max(travel_time, D_min), D_max)
			#new_time = min(last_segment[2] + travel_time, t_max-1)
			#route.append([last_segment[1], z, new_time])
		ptemp.append(route)
	Particles[t] = ptemp


	
def Weigh():
	global Particles
	global Weights
	for i in range(N):
		Weights[i] = WeighRoute(Particles[i])

def Prob(route_curr, route_next):
	curr_intersection = route_curr[-1][1]
	next_intersection = route_next[-1][1]
	#calculate the probability of getting from curr_intersection to next_intersection
	if segments[curr_intersection][next_intersection] == 1:
		return 1.0/sum(segments[curr_intersection])
	else:
		return 0
		
def WeighSmooth(t):
	global Particles
	global Weights
	for i in range(N):
		w = 0
		for j in range(N):
			w += Prob(Particles[t][i], Particles[t+1][j])
		Weights[i] = w
			
def Resample(t):
	global Particles
	ptemp = []
	index = int(random.random() * N)
	beta = 0.0
	mw = max(Weights)
	for i in range(N):
		beta += random.random() * 2.0 * mw
		while beta > Weights[index]:
			beta -= Weights[index]
			index = (index + 1) % N
		ptemp.append(Particles[t][index])
	Particles[t] = ptemp

def ReadRoute(route):
	l = []
	for i in route:
		l.append(str(i[0]))
	l.append(str(i[1]))
	#l.append(str(i[2]))
	return l
		
init_segments()
init_ref_array()
input_track()


#Initialization
Particles = []
for i in range(T):
	Particles.append([])

for i in range(N):
	while True:
		x = random.randrange(MAX_INTERSECTIONS)
		y = random.randrange(MAX_INTERSECTIONS)
		if segments[x][y] == 1 and x != y:
			break;
	Particles[0].append([[x,y,0]])
	#Particles.append([[5,4,0]])
	Weights.append(0)
#pprint(Particles[0])
	
for t in range(1,T):
	print "Filter Round " + str(t)
	Move(t)
	#Weigh()
	Resample(t)
	PrintHistogram(Particles[t],t+3)
for t in reversed(range(0,T-1)):
	print "Smooth Round " + str(t)
	WeighSmooth(t)
	Resample(t)
	PrintHistogram(Particles[t],T+3)
#pprint(Particles)

#Rank()