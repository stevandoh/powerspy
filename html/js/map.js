var SOUTH_BAY_LAT = 37.38;
var SOUTH_BAY_LON = -122;

// colors can be specified by either names or RGB numbers
// such as '#FF0000'
// var COLOR_FIELD = 'Signal';
var COLOR_FIELD = 'NaN';
var DEFAULT_COLOR = 'red';

var map = null;
var mapCenter = null;
var lastData = null;

var lastPath = null;

function init_last_path() {
    return {
        lines: [],
        startMarker: null,
        endMarker: null
    };
}

function filter_nogps_data(data) {
    var filteredData = jQuery.grep(data, function (row) {
        return (-1.0 !== row.Latitude || -1.0 !== row.Longitude);
    });

    return filteredData;
} // filter_nogps_data

function value_mapping(value) {
    return Math.log(value);
}

function find_min_max(values) {
		var minval = Infinity;
		var maxval = -Infinity;

		for (var i = 0; i < values.length; ++i) {
				if (values[i] < minval) {
						minval = values[i];
				}

				if (values[i] > maxval) {
						maxval = values[i];
				}
		}

		return [minval, maxval];
}

function find_values_range(values) {
		var min_max = find_min_max(values);
    return min_max[1] - min_max[0];
}

/*
  Get the indices for which data value changes by more
  than EPS.
*/ 
function get_value_change_indices(values) {
    
    var EPS = find_values_range(values) / 10;
    var indices = [];
    
    var last_change_ind = 0;
    for (var i = 1; i < values.length; ++i) {
        var one = values[i];
        var two = values[last_change_ind];
        if (Math.abs(one - two) > EPS) {
            indices.push(i);
            last_change_ind = i;
        }
    }

    return indices;
} // end of get_value_change_indices

/*
  Get latitude/longitude coordinates from Json data.
 */
function get_path_coords(data) {
    var pathCoords = [];
    var filteredData = filter_nogps_data(data);

    for (var i = 0; i < filteredData.length; i++) {
        var latitude = filteredData[i].Latitude;
        var longitude = filteredData[i].Longitude;

        pathCoords.push(new google.maps.LatLng(latitude, longitude));
    }

    return pathCoords;
}

/*
  Get all values for a given column.
*/
function get_column_values(rows, colName) {
    // $.map doesn't work for very large data arrays
    values = [];
    for (var i = 0; i < rows.length; ++i) {
        values.push(rows[i][colName]);
    }

    return values;
} // end of get_column_values

/*
 Calculate the coordinates of the center of the path
 described by the Json data.
 */
function get_path_center(data) {
    var filteredData = filter_nogps_data(data);
    if (0 == filteredData.length) {
        console.warn("Empty data");
        return null;
    }

    var latValues = get_column_values(filteredData, 'Latitude');
    var lonValues = get_column_values(filteredData, 'Longitude');

		var lat_min_max = find_min_max(latValues);
    var latMax = lat_min_max[1];
    var latMin = lat_min_max[0];
		var lon_min_max = find_min_max(lonValues);
    var lonMax = lon_min_max[1];
    var lonMin = lon_min_max[0];

    var centerLat = (latMax + latMin) / 2;
    var centerLon = (lonMax + lonMin) / 2;

    return {
        lat: centerLat,
        lon: centerLon
    };
} // end of get_path_center

function create_map() {
    var map_canvas = document.getElementById('map_canvas');

    var map_options = {
        center: new google.maps.LatLng(SOUTH_BAY_LAT, SOUTH_BAY_LON),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    return new google.maps.Map(map_canvas, map_options);
} // end of create_map

function put_marker(map, coords, title, icon) {
    var marker = new google.maps.Marker({
        position: coords,
        map: map,
        title: title
    });

    if (icon) {
        marker.setIcon(icon);
    }

    return marker;
} // end of put_marker

function draw_path_part(map, pathCoords, color) {
    var path = new google.maps.Polyline({
        path: pathCoords,
        geodesic: true,
        strokeColor: color,
        strokeOpacity: 1,
        strokeWeight: 5,
        map: map
    });

    return path;
} // end of draw_path_part

function get_column_min_max(data, field) {
    var filteredData = jQuery.grep(data, function(row) {
        return row[field] != -1;
    });

    var values = get_column_values(filteredData, field);
		var min_max = find_min_max(values);

    return {
        min: min_max[0],
        max: min_max[1]
    };
} // end get_column_min_max

/*
  Visualize data on map. Color the path line according to
  the values of a field specified by COLOR_FIELD.
 */
function visualize_data(map, data) {
    var color = DEFAULT_COLOR;
    var pathCoords = get_path_coords(data);
    if (0 == pathCoords.length) {
        console.warn("Empty data");
        return;
    }

    var startMarkerCoords =
        new google.maps.LatLng(pathCoords[0].k, pathCoords[0].D);

    var endMarkerCoords =
        new google.maps.LatLng(pathCoords[pathCoords.length - 1].k,
            pathCoords[pathCoords.length - 1].D);

    lastPath.startMarker = put_marker(map, startMarkerCoords, "Start", 'images/startmarker.png');
    lastPath.endMarker = put_marker(map, endMarkerCoords, "Finish");

    var values = get_column_values(data, COLOR_FIELD);
    for (var i = 0; i < values.length; ++i) {
        values[i] = value_mapping(values[i]);
    }

    var valuesForMinMax = jQuery.grep(values, function(val) {
        return val >= 0;
    });
		var min_max = find_min_max(valuesForMinMax);
    var minval = min_max[0];
    var maxval = min_max[1];
    var range = maxval - minval;

    var valueChangeIndices = get_value_change_indices(values);
    if (valueChangeIndices.length == 0 || COLOR_FIELD == 'NaN') {
        lastPath.lines.push(draw_path_part(map, pathCoords, DEFAULT_COLOR));
        return;
    }

    var prevIndex = 0;
    for (var i = 0; i < valueChangeIndices.length; ++i) {
        var pathPart = [];
        for (var j = prevIndex; j < valueChangeIndices[i] - 1; ++j) {
            pathPart.push(pathCoords[j]);
        }

        var colVal = values[prevIndex];
        if (colVal < 0) {
            color = DEFAULT_COLOR;
        } else {
//            colVal = value_mapping(colVal);
            colVal = colVal - minval;
            var g = Math.floor(255 * colVal / range);
            var r = Math.floor(255 * (range - colVal) / range);
            color = "rgb(" + r + ", " + g + ", 0)";
        }

        try {
            lastPath.lines.push(draw_path_part(map, pathPart, color));
            prevIndex = valueChangeIndices[i];
        } catch (e) {
            console.log(e);
        }
    } // for each new value
} // end of visualize_data

/*
  Remove currently drawn path.
*/
function clear_path(path) {
    // remove markers
		if (path.startMarker != null) {
    	path.startMarker.setMap(null);
		}
		if (path.endMarker != null) {
    	path.endMarker.setMap(null);
		}

    for (var i = 0; i < path.lines.length; ++i) {
        path.lines[i].setMap(null);
    }
} // end of clear_path

/*
  Normalize values array by subtracting the mean for
  each value and dividing by the standard deviation.
*/
function normalize_values(values) {
    // calculate data mean
    var mean = values.reduce( function(sum, val) {
	return sum + val;
    }, 0);

    mean = mean / values.length;
    values = values.map( function(val) {
	   return val - mean;
    });

    // calculate standard deviation
    var variance = values.reduce( function(sum, val) {
	   return sum + val * val;
    }, 0);

    values = values.map( function(val) {
	   return val / Math.sqrt(variance);
    });

    return values;
} // end of normalize_values

/* 
   Plots data on char container div.
 */
function plot_data(data) {
    if ('NaN' == COLOR_FIELD || 'None' == COLOR_FIELD) {
        return;   
    }
    
    var header = ['Time', COLOR_FIELD];
    
    var time = get_column_values(data, 'Time');
    var values = get_column_values(data, COLOR_FIELD);

    // normalize Current data
    if (COLOR_FIELD == 'Current') {
	   values = normalize_values(values);
    }
    
    var arrayData = [header];
    
    var STEP = 10;
    for (var i = 0; i < time.length; i = i + STEP) {
        arrayData.push([time[i], values[i]]);
    }
    
    var chartData = google.visualization.arrayToDataTable(arrayData);
    
    var options = {
      legend: { position: 'bottom' }
    };
    var chart = new google.visualization.LineChart(document.getElementById('plot_container'));
    chart.draw(chartData, options);
} // end of plot_data

/*
  Process data in JSON format and show on map.
 */
function process_data(data) {
    plot_data(lastData);
    
    if (null == map) {
        // no map created yet, let's create one
        map = create_map();
    }

    var pathCenter = get_path_center(data);
    if (null != pathCenter) {
        mapCenter = pathCenter;
        map.setCenter(new google.maps.LatLng(mapCenter.lat, mapCenter.lon));
    }

    visualize_data(map, data);
}

function on_combo_change(option) {
    COLOR_FIELD = option;

    if (null != lastPath) {
        clear_path(lastPath);
    }

    lastPath = init_last_path();

    if (null != lastData) {
        process_data(lastData);
    }
} // end of on_combo_change

function csv2json(csvData) {
    return csvjson.csv2json(csvData, {
        delim: "\t"
    });
}

function process_csv_data(data) {
    var jsonObj = csv2json(data);

    var combo = document.getElementById("field_combo");
    // remove existing column names
    combo.options.length = 0;

    var noneOpt = document.createElement("option");
    noneOpt.text = "None";
    noneOpt.value = "NaN";
    combo.add(noneOpt, null);

    var selInd = 0;
    // add field names to combo
    for (var i = 0; i < jsonObj.headers.length; ++i) {
        var option = document.createElement("option");
        option.text = jsonObj.headers[i];
        if (option.text == 'Time') {
            continue;
        }
        option.value = option.text;
        combo.add(option, null);
        if (option.text == COLOR_FIELD) {
            selInd = i;
        }
    }

    combo.selectedIndex = selInd;

    lastPath = init_last_path();
    lastData = jsonObj.rows;

    process_data(lastData);
} // end process_csv_data

/*
 Fetches CSV data from URL, converts to JSON
 and shows on the map.
 */
function load_data(url) {
    $.ajax(url, {
        success: function(data) {
            console.log("Loaded CSV data");
            process_csv_data(data);
        },
        error: function() {
            console.error("Failed loading data from " + url);
        }
    });
} // end of load_data

/*
 Fit map canvas size to window size.
 */
function fit_map() {
    $('#map_canvas').height($(map_container).height());
    $('#map_canvas').width($(map_container).width());
}

function handle_drag(e) {
    e.stopPropagation();
    e.preventDefault();
    document.getElementById('drop_container').style.display = 'block';
    return false;
}

function handle_file(file) {
    var reader = new FileReader();
    reader.onload = function(e) {
        process_csv_data(e.target.result);
    };

    reader.onerror = function(e) {
        console.error('Failed reading file ' + file.name);
    }

    reader.readAsText(file);
} // end of handle_file

function handle_drop(e) {
    e.preventDefault();
    e.stopPropagation();

    var files = e.dataTransfer.files;
    if (0 == files.length) {
        // no files to process
        // propagate event
        return true;
    }

    for (var i = 0; i < files.length; i++) {
        handle_file(files[i]);
    }

    // prevent event propagation
    return false;
} // end of handle_drop

/*
 Set callback functions to handle drag-over and drop events.
 */
function init_events() {
    var map_canvas = document.getElementById("map_canvas");
    var drop_container = document.getElementById("drop_container");

    [map_canvas, drop_container].forEach(function(container) {
        container.addEventListener('drop', handle_drop, false);
        container.addEventListener('dragover', handle_drag, false);
    });

    $("select").change(function() {
        var option = $("select option:selected").text();
        on_combo_change(option);
    });
}

function initialize() {
    init_events();

    mapCenter = {
        lat: SOUTH_BAY_LAT,
        lon: SOUTH_BAY_LON
    };

    lastPath = init_last_path();

    // fit map canvas to window size
    $(window).resize(function() {
        fit_map();
        if (null != map) {
            map.setCenter(new google.maps.LatLng(mapCenter.lat, mapCenter.lon));
        }
    });

    fit_map();
    create_map(null);
} // end of initialize

google.maps.event.addDomListener(window, 'load', initialize);
google.load("visualization", "1", { packages:["corechart"]});
