#!/bin/sh

route_prefix=$1
intersections_file=$2

devices=(0076296b4ed758ef
	0094e779d7d1841f
	00a697fa469633a5
	042335963089f20b
	04dc22d4dad7e4ce
	087a1d930591f4ca
	2c8ed07b808fe468
	3230df8e10eb10ed)

for d in ${devices[*]}; do 
	echo Processing logs for device $d
	for f in $(ls ../BatLevelLogs/$d/$route_prefix*); do
		echo $f; 
		../src/py/routechooper.py $f $intersections_file $d/ ;
	done ; 
done
