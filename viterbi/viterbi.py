import time
import mlpy
import numpy
import glob
import os
from itertools import product
from pprint import pprint
import sys

t_max = -1
start_line = 1472
D_min = 5
D_max = 60
MAX_INTERSECTIONS = 8

indirs = [ "../cut-segments/segments/AhuzaOct31-20141031_180252/smooth", "../cut-segments/segments/AhuzaOct20-20141020_093624/smooth" ]

trackfile = "../cut-segments/vrd-yotam-horev-pica-vrd-20141031_191430.csv"

ref_array = {}


#print prob 

segments = [ [ 0 for x in xrange(MAX_INTERSECTIONS) ] for x in xrange(MAX_INTERSECTIONS) ]
track = []
times_access = {}
norm_subtracks  = {}

print time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
def init_segments():
	segments[1][7] = segments[1][2] = 1
	segments[2][1] = segments[2][3] = segments[2][4] = 1
	segments[3][2] = segments[3][4] = 1
	segments[4][2] = segments[4][3] = segments[4][5] = 1
	segments[5][4] = segments[5][6] = 1
	segments[6][4] = segments[6][5] = segments[6][7] = 1
	segments[7][1] = segments[7][6] = 1

def init_ref_array():
	for x, y, z in product(xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS)):
		if segments[x][y] == 1 and segments[y][z] == 1 and x != z:
			filename = str(x) + "_" + str(y) + "_" + str(z)
			powerlist = []
			for dir in indirs:
				for sfile in glob.glob(os.path.join(dir, filename + "*.csv")):
					power = []
					#filepath = os.path.join(dir, sfile)
					sf = open(sfile, 'r')
					for line in sf:
						values = line.split('\t')
						try:
							time = float(values[0]) #just to check if this is a valid line
						except ValueError:
							continue
						power.append(values[1])
					sf.close()
					powerlist.append(power)
			ref_array[filename] = powerlist			

def input_track():
	global t_max
	f = open(trackfile, 'r')
	next_time = 0
	start_time = None
	time_quantum = 3000 # in mili-seconds
	i = 0
	for line in f:
		values = line.split('\t')
		try:
			time = float(values[0])
		except ValueError:
			continue
		track.append(float(values[2])*float(values[3]))
		if i > start_line:
			if start_time is None:
				start_time = time
			if time - start_time >= next_time*time_quantum:
				times_access[next_time] = i
				next_time += 1
		i += 1
	t_max = int(next_time - 1)
	print t_max
	
	#normalize each possible interval in the track
	for t_start in range(t_max-D_min):
		print str(t_start)
		for t_end in range(t_start + D_min, min(t_max, t_start + D_max)):
			timename = str(t_start)+ "-" + str(t_end)
			start_i = times_access[t_start]
			end_i = times_access[t_end]
			subtrack = track[start_i:end_i]
			#Normalize
			mean = numpy.mean(numpy.array(subtrack))
			std = numpy.std(numpy.array(subtrack))
			for i in range(len(subtrack)):
				subtrack[i] = (subtrack[i] - mean)/std
			#cut percentile
			perc_avg_power = numpy.percentile(numpy.array(subtrack), 90) #90% gave the best results
			for i in range(len(subtrack)):
				if subtrack[i] < perc_avg_power:
					subtrack[i] = 0
			norm_subtracks[timename] = subtrack
	#pprint(norm_subtracks)

prob_o = {}
def init_calc_prob_o():
	print "init_calc_prob_o"
	for x, y, z in product(xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS)):
		if segments[x][y] == 1 and segments[y][z] == 1 and x != z:
			segmentname = str(x) + "_" + str(y) + "_" + str(z)
			print segmentname
			powerreflist = ref_array[segmentname]
			time_dict = {} #this dictionary will contain for each time interval its distance to the current profile segment
			for t_start in range(t_max-D_min):
				print "\n" + str(t_start)
				for t_end in range(t_start + D_min, min(t_max, t_start + D_max)):
					sys.stdout.write("\t" + str(t_end))
					timename = str(t_start)+ "-" + str(t_end)
					subtrack = norm_subtracks[timename]
					min_dist = float("inf")
					for powerref in powerreflist:
						dist = mlpy.dtw_std(subtrack, powerref)/min(len(subtrack), len(powerref))
						sys.stdout.write('.')
						min_dist = min(min_dist, dist)
					time_dict[timename] = 1/min_dist
			prob_o[segmentname] = time_dict
			
	#normalize the distances so it can be considered as probabilities
	for t_start in range(t_max-D_min):
		for t_end in range(t_start + D_min, min(t_max, t_start + D_max)):
			timename = str(t_start)+ "-" + str(t_end)
			print timename
			max_inv_dist = 0
			for x, y, z in product(xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS)):
				if segments[x][y] == 1 and segments[y][z] == 1 and x != z:
					segmentname = str(x) + "_" + str(y) + "_" + str(z)
					max_inv_dist = max(max_inv_dist, prob_o[segmentname][timename])
			for x, y, z in product(xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS), xrange(MAX_INTERSECTIONS)):
				if segments[x][y] == 1 and segments[y][z] == 1 and x != z:
					segmentname = str(x) + "_" + str(y) + "_" + str(z)
					prob_o[segmentname][timename] = prob_o[segmentname][timename]/max_inv_dist
				
	
				


		
	
def calc_prob_o(x, y, z, t_start, t_end):
		
	
	return random.uniform(0,1)
			
def cal_prob(dep, t, y, z):
	indent = ""
	for i in range(dep):
		indent = indent + "_"
	#print indent + "(" + str(t) + ", " + str(y) + ", " + str(z) + ")"
	if t < D_min and t > 0:
		return 0.0
	if prob[t][y][z][0] != 0:
		return prob[t][y][z][1]
	max_p = 0.0
	arg_max_p = [-1,-1]
	for x in range(MAX_INTERSECTIONS):
		if segments[x][y] != 1:
			continue
		for D in range(D_min, min(D_max,t)+1):
			p = cal_prob(dep+1,t-D, x, y) * (1.0/sum(segments[y])) * calc_prob_o(x, y, z, t-D, t)
			if p >= max_p:
				max_p = p
				arg_max_p = (x, D)
	
	prob[t][y][z][0] = 1
	prob[t][y][z][1] = max_p
	prob[t][y][z][2] = arg_max_p[0]
	prob[t][y][z][3] = arg_max_p[1]
	#print "(" + str(t) + ", " + str(y) + ", " + str(z) +") = " + str(max_p)
	return max_p

init_segments()
#print segments
init_ref_array()
				
total_segments = 0			 
for x in range(MAX_INTERSECTIONS):
	total_segments = total_segments + sum(segments[x])
print "Total number of segments: " + str(total_segments)
print "input track"
input_track()
#prob = [ [ [ [ -1.0 for x in xrange(3) ] for x in xrange(MAX_INTERSECTIONS) ] for x in xrange(MAX_INTERSECTIONS) ] for x in xrange(t_max) ]
prob = numpy.zeros((t_max, MAX_INTERSECTIONS, MAX_INTERSECTIONS, 4)) #prob[time, x, y] = (is_calculated_yet?, prob, arg_max_x, arg_max_D)
init_calc_prob_o()

'''
for x in range(MAX_INTERSECTIONS):
	for y in range(MAX_INTERSECTIONS):
		if segments[x][y] == 1:
			prob[0][x][y][0] = 1
			prob[0][x][y][1] = 1.0/total_segments
			prob[0][x][y][2] = -1
			prob[0][x][y][3] = -1

print time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

max_p = 0
arg_max_p = (0,0)
for x in range(MAX_INTERSECTIONS):
	for y in range(MAX_INTERSECTIONS):
		if segments[x][y] == 1:
			p = cal_prob(0, t_max-1, x, y)
			if p > max_p:
				max_p = p
				arg_max_p = (x, y)
			#print pr

print max_p
t = t_max - 1
x = arg_max_p[0]
y = arg_max_p[1]
route = str(y)
while t > 0:
	D = prob[t][x][y][3]
	route = route + "-(" + str(D) + ")-" + str(x)
	print route
	prev = int(prob[t][x][y][2])
	y = x
	x = prev
	t = t-D 
route = route + "-" + str(x)
print route
	
print time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
'''			


				
