package com.securitylab.getbatterylevel;

public class Constants {
	// Hardware status files
	public static final String SYS_CLASS_VOLTAGE_NOW = "/sys/class/power_supply/battery/voltage_now";
	public static final String SYS_CLASS_CURRENT_NOW = "/sys/class/power_supply/battery/current_now";
	public static final String SYS_CLASS_TEMP = "/sys/class/power_supply/battery/temp";
	public static final String SYS_CLASS_CAPACITY = "/sys/class/power_supply/battery/capacity";
	public static final String TAG = "GetBatteryLevel";
	
	// Intent extras
	public static final String EXTRA_GPS = "com.securitylab.getbatterylevel.cbGPS";
	public static final String EXTRA_SIGNAL_STRENGTH = "com.securitylab.getbatterylevel.cbSignalStrength";
	public static final String EXTRA_BATTERY = "com.securitylab.getbatterylevel.cbBattery";
	public static final String EXTRA_SAVE_LOG = "com.securitylab.getbatterylevel.cbSaveLog";
	public static final String EXTRA_COMMENT = "com.securitylab.getbatterylevel.commentTxt";
	public static final String EXTRA_RECORD =  "com.securitylab.getbatterylevel.RECORD";
	
	public static final String ACTION_SEND_RECORD = "com.securitylab.getbatterylevel.SEND_RECORD";

}
