package com.securitylab.getbatterylevel;


import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

public class BackgroundRecorder extends Service {
	
	private ListenerThread m_listenerThread;
	private NetworkThread m_networkThread;
	private boolean m_running = false;
	private Intent m_workIntent = null;
	private final IBinder m_binder = new BackgroundRecorderBinder();
	
	public class BackgroundRecorderBinder extends Binder {
		BackgroundRecorder getService() {
			return BackgroundRecorder.this;
		}
	}

    public BackgroundRecorder() {
		super();
	}
    
    public boolean isRunning()
    {
    	return m_running;
    }
    
    public Intent getWorkIntent() {
    	return m_workIntent;
    }
    
	@Override
	public int onStartCommand(Intent workIntent, int flags, int startId) {
		if (m_running) {
			return START_REDELIVER_INTENT;
		}
		
		m_workIntent = workIntent;
		startListenerThread();
		Log.d(Constants.TAG, "Started Listener thread");
		
		TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		if (tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT)
		{
			// if there is no SIM we don't try to transmit data since 
			// there is no 3G data connection
		    m_networkThread = new NetworkThread();
		    m_networkThread.start();
		    Log.d(Constants.TAG, "Started Network thread");
		}
	    
	    m_running = true;
	    
		// Make foreground service
		Notification notification = ServiceNotification.buildNotification(this);
		startForeground(ServiceNotification.NOTIFICATION_ID, notification);
	    
		return START_REDELIVER_INTENT;
    }
	
	private void startListenerThread() {
		Bundle extras = m_workIntent.getExtras();
	    final boolean cbGPS_value  = extras.getBoolean(Constants.EXTRA_GPS);
	    final boolean cbSignalStrength_value = extras.getBoolean(Constants.EXTRA_SIGNAL_STRENGTH);
	    final boolean cbBattery_value = extras.getBoolean(Constants.EXTRA_BATTERY);
	    final boolean cbSaveLog_value = extras.getBoolean(Constants.EXTRA_SAVE_LOG);
	    final String comment = extras.getString(Constants.EXTRA_COMMENT);
	    
	    m_listenerThread = new ListenerThread(this, cbGPS_value, cbBattery_value, 
	    		cbSignalStrength_value, cbSaveLog_value, comment);
	    m_listenerThread.start();
	}
	
	@Override
    public void onDestroy() {
		Log.d(Constants.TAG, "Service is quitting.");
		
		try {
			if (m_listenerThread != null) {
				m_listenerThread.quit();
				m_listenerThread.join();
			}
			
			if (m_networkThread != null) {
				m_networkThread.quit();
				m_networkThread.join();
			}
		}
		catch (InterruptedException e) {
			Log.e(Constants.TAG, "Waiting for threads interrupted");
			e.printStackTrace();
		}
		
		m_running = false;
		
		super.onDestroy();
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return m_binder;
	}
	
} // end of BackgroundRecorder