#!/usr/bin/env python

import sys
import pandas
import os
import progress.bar

global log_dir
global progress_bar

def get_route_time(row):
    global log_dir
    global progress_bar

    file = os.path.join(log_dir, row['Device'], row['Filename'])
    # print 'Processing', file
    route_log = pandas.read_table(file)
    progress_bar.next()
    # return len in seconds
    return (route_log['Time'][len(route_log['Time'])-1] - route_log['Time'][0]) / 1000

def main():
    global log_dir
    global progress_bar

    loglist_filename = sys.argv[1]
    output_file = sys.argv[2]
    log_dir = os.path.dirname(loglist_filename)
    log_list = pandas.read_table(loglist_filename)
    progress_bar = progress.bar.Bar('Processing', max=len(log_list))
    log_list['RouteTime'] = log_list.apply(get_route_time, 1)
		print
    log_list.to_csv(output_file, index=False, sep='\t')

if __name__ == '__main__':
    main()
